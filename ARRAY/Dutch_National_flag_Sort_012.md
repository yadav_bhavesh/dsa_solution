# CODE
```
#include <bits/stdc++.h> 
using namespace std; 

void sort012(int a[], int arr_size) 
{ 
    int lo = 0; 
    int hi = arr_size - 1; 
    int mid = 0; 
  
    // Iterate till all the elements 
    // are sorted 
    while (mid <= hi) { 
        switch (a[mid]) { 
  
        // If the element is 0 
        case 0: 
            swap(a[lo++], a[mid++]); 
            break; 
  
        // If the element is 1 . 
        case 1: 
            mid++; 
            break; 
  
        // If the element is 2 
        case 2: 
            swap(a[mid], a[hi--]); 
            break; 
        } 
    } 
} 

// Function to print array arr[] 
void printArray(int arr[], int arr_size) 
{ 
	// Iterate and print every element 
	for (int i = 0; i < arr_size; i++) 
		cout << arr[i] << " "; 
} 

// Driver Code 
int main() 
{ 
	int n;
	cin >> n;

	int arr[n];

	for(int i = 0; i < n;i++){
		cin >> arr[i];
	}

	sort012(arr, n); 

	cout << "array after segregation " << endl; 

	printArray(arr, n); 

	return 0; 
} 

```

# I/O
```
Input  : 
8
0 2 1 2 0 2 0 0

Output : 
array after segregation 
0 0 0 0 1 2 2 2 
```
## Time Complexity of sort012 func: O(n*logn)