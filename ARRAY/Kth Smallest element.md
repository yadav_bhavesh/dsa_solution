# CODE
```
#include<bits/stdc++.h>
using namespace std;
int kth_element(int arr[], int n, int k){

	sort(arr, arr+n);

	return arr[k-1];
}

int main(){
	int n, t, k;
	cin >> t;

	for(int i = 0; i < t; i++){
		
		cin >> n;
		
		int arr[n];

		for(int j = 0; j < n; j++){
			cin >> arr[j];
		}

		cin >> k;

		cout << kth_element(arr, n, k) << endl;
	}
    return 0;
}
```

- The first line of input contains an integer *T*, denoting the number of testcases. Then T test cases follow. 
- Each test case consists of three lines. First line of each testcase contains an integer *N* denoting size of the array. 
- Second line contains *N* space separated integer denoting elements of the array. 
- Third line of the test case contains an integer *K*.

# I/O
```
Input  : 
2
6
7 10 4 3 20 15
3
5
7 10 4 20 15
4
Output : 
7
15
```
## Time Complexity of kth_element func: O(n*logn)