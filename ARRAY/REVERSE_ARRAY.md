# CODE
```
#include<bits/stdc++.h>
using namespace std;

int main(){
    int n;
	cin >> n;

	int arr[n];

	for(int i = 0; i < n; i++){
		cin >> arr[i];
	}

	for(int i = n-1; i>=0; i--){
		cout << arr[i] <<" ";
	}
    return 0;
}
```

# I/O
```
Input  : arr[] = {1, 2, 3}
Output : arr[] = {3, 2, 1}

Input :  arr[] = {4, 5, 1, 2}
Output : arr[] = {2, 1, 5, 4}
```
## Time Complexity: O(n)