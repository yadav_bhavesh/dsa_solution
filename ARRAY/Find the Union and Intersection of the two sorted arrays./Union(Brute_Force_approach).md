# CODE
**<b>Union of two Sorted arrays</b>**
```
#include <iostream> 
#include <vector>
using namespace std; 

void Union(int A[], int B[], int n, int m){

	vector <int> V;

	for(int i = 0; i < n; i++){

		V.push_back(A[i]);
	}
    
    for(int j = 0; j < m; j++){

    	for(int k = 0; k < V.size(); k++){

    		if(B[j] == V[k]){

    			break;
    		}
    		else{                                

    			if(k == V.size()-1){

    				V.push_back(B[j]);
    			}
    		}
    	}
    }

    for(auto x : V){
    	cout << x <<" ";
    }
}

int main() 
{   
	int n, m;
	cin >> n >> m;

	int A[n];
	int B[m];

	for(int i = 0; i < n; i++){

		cin >> A[i];
	}

	for(int j = 0; j < m; j++){

		cin >> B[j];
	}	

	Union(A, B, n, m);
	return 0; 
} 

```

# I/O
```
Input  : 
6 3
1 6 25 32 54 85
4 5 6

Output : 
array after Union
1 6 25 32 54 85 4 5  
```
## Time Complexity of Union func: O(j*k)