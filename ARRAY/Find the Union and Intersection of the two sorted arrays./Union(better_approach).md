# CODE
**<b>Union of two Sorted arrays</b>**
```
#include <iostream> 
using namespace std; 

void printUnion(int arr1[], int arr2[], int m, int n) 
{ 
    int i = 0, j = 0; 
    while (i < m && j < n) { 
        if (arr1[i] < arr2[j]) 
            cout << arr1[i++] << " "; 
  
        else if (arr2[j] < arr1[i]) 
            cout << arr2[j++] << " "; 
  
        else { 
            cout << arr2[j++] << " "; 
            i++; 
        } 
    } 
  
    /* Print remaining elements of the larger array */
    while (i < m) 
        cout << arr1[i++] << " "; 
  
    while (j < n) 
        cout << arr2[j++] << " "; 
} 

int main() 
{ 
	int n, m;
	cin >> n >> m;

	int A[n];
	int B[m];

	for(int i = 0; i < n; i++){

		cin >> A[i];
	}

	for(int j = 0; j < m; j++){

		cin >> B[j];
	}	

	printUnion(A, B, n, m);
	return 0; 
} 

```

# I/O
```
Input  : 
5 3 
1 2 3 4 5
4 5 6

Output : 
array after Union
1 2 3 4 5 6 
```
## Time Complexity of Union func: O(N+M)