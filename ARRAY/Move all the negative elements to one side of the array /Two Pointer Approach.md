# Code
```
#include <iostream> 
using namespace std; 

void rearrange(int a[], int n){

	int l = 0;
	int r = n-1;

	while(l<=r){

		if(a[l] < 0 && a[r] < 0){

			l++;
		}
		else if(a[l] > 0 && a[r] < 0){

			swap(a[l], a[r]);
			l++;
			r--;
		}
		else if(a[l] > 0 && a[r] > 0){
			r--;
		}
		else{
			l++;
			r--;
		}
	}
}

void printarr(int a[], int n){

	for(int i = 0; i < n; i++){

		cout << a[i] <<" ";
	}
}

int main() 
{   
    int n;
    cin >> n;

    int a[n];
    for(int i = 0; i < n; i++){
    	cin >> a[i];
    }

    rearrange(a, n);

    printarr(a, n);
	return 0; 
} 
```
# I/O
```
Input  : 
9
-12 11 -13 -5 6 -7 5 -3 -6

Output : 
-12 -6 -13 -5 -3 -7 5 6 11 
```
## Time Complexity of rearrange func: O(n)