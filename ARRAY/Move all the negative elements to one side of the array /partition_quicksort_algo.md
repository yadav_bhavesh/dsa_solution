# CODE
```
#include <iostream> 
using namespace std; 

void rearrange(int a[], int n){

	int j = 0;

	for(int i = 0; i < n; i++){

		if(a[i] < 0){

			if(i!=j){

				swap(a[i], a[j]);

			}

			j++;
		}
	}
}

void printarr(int a[], int n){

	for(int i = 0; i < n; i++){

		cout << a[i] <<" ";
	}
}

int main() 
{     
    int n;
    cin >> n;

    int a[n];
    for(int i = 0; i < n; i++){
    	cin >> a[i];
    }

    rearrange(a, n);

    printarr(a, n);
	return 0; 
} 

```

# I/O
```
Input  : 
9
-12 11 -13 -5 6 -7 5 -3 -6

Output : 
-12 -13 -5 -7 -3 -6 5 6 11 
```
## Time Complexity of rearrange func: O(n)