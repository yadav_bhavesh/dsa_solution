# CODE
```
#include<bits/stdc++.h>
using namespace std;

/* Here in func if we have to pass parameter as array then we have write it
   as arr[] otherwise show array subscript error*/

int max_value(int arr[], int n){

	int max = arr[0];

	for(int i = 1; i < n; i++){
		if(arr[i] > max){
			max = arr[i];
		}
	}

	return max;
}

int min_value(int arr[], int n){

	int min = arr[0];

	for(int i = 1; i < n; i++){
		if(arr[i] < min){
			min = arr[i];
		}
	}

	return min;
}

int main(){

	int n;
	cin >> n;

	int arr[n];

	for(int i = 0; i < n; i++){
		cin >> arr[i];
	}

	cout << "Max value in array: "<< max_value(arr, n) << endl;
	cout << "Min value in array: "<< min_value(arr, n) << endl;

    return 0;
}
```

# I/O
```
Input  : arr[] = {10, 11, 88, 2, 12, 120}
Output : Max value in array: 120
         Min value in array: 2
```
## Time Complexity: O(n)